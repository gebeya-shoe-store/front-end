import apiClient from "@/resources/apiClient";

export default {
    
    check_out: (_, payload) => {
        return new Promise((resolve, reject) => {
            apiClient.cart.check_out(payload)
            .then(response => {
                resolve( response.data );
            })
            .catch(e => {
                reject(e);
            })
        });
    },

};