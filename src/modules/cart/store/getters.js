export default {
    total_items: state => {
        let count = 0;
        for (let cart of state.cart) {
            count += cart.amount;
        }
        return count;
    },
    total_price: state => {
        let count = 0;
        for  (let cart of state.cart) {
            count += cart.amount * Number(cart.item.price);
        }
        return count.toFixed(2);
    },
    cart: state => state.cart,
};