export default {
    ADD_TO_CART: (state, data) => {
        let index = state.cart.findIndex(x => x.itemId == data.itemId);
        if (index !== -1) {
            state.cart[index].amount += Number(data.amount);
        } else {
            state.cart.push(data);
        }
    },
    REMOVE_CART: (state, id) => {
        state.cart = state.cart.filter(x => x.itemId !== id);
    },
    RESET_CART: (state) => state.cart = [],
};