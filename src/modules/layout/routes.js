import Layout from "./views/Layout";
import Home from "../../modules/shoes/views/Home";
import ItemDetail from "../../modules/shoes/views/ItemDetail";
import Cart from "../../modules/cart/views/Cart";

export default [

    {
        path: "/",
        component: Layout,
        children: [
            {
                path: "",
                name: "home",
                components: {
                    default: Home,
                }
            }
        ],
    },

    {
        path: "/item/:id",
        component: Layout,
        children: [
            {
                path: "",
                name: "detail",
                components: {
                    default: ItemDetail,
                }
            }
        ],
    },

    {
        path: "/cart",
        component: Layout,
        children: [
            {
                path: "",
                name: "cart",
                components: {
                    default: Cart,
                }
            }
        ],
    },

];