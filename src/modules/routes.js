import { routes as shoes } from "./shoes";
import { routes as layout } from "./layout";
import { routes as cart } from "./cart";

export default [ ...shoes , ...layout, ...cart];