import apiClient from "@/resources/apiClient";

export default {

    get_items: (_, payload) => {
        return new Promise((resolve, reject) => {
            apiClient.shoes.get_shoes(payload)
            .then(response => {
                resolve( response.data );
            })
            .catch(e => {
                reject(e);
            })
        });
    },

    shoes_filters: ({ commit }) => {
        return new Promise((resolve, reject) => {
            apiClient.shoes.shoes_filters()
            .then(response => {
                commit('SET_CURRENCY', response.data.currencies);
                resolve( response.data );
            })
            .catch(e => {
                reject(e);
            })
        });
    },

    get_shoe_detail: (_, id) => {
        return new Promise((resolve, reject) => {
            apiClient.shoes.get_shoe_detail(id)
            .then(response => {
                resolve( response.data );
            })
            .catch(e => {
                reject(e);
            })
        });
    },
};