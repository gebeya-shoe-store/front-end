import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

export default {
    namespaced: true,
    state: {
        currency: null,
    },
    getters,
    actions,
    mutations,
}