import { store as shoes } from "./shoes";
import { store as layout } from "./layout";
import { store as cart } from "./cart";

export default { shoes, layout, cart };