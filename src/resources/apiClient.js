import http from "./http";
import urls from "./urls";

export default {
    shoes: {
        get_shoes(query) {
            return http.get(urls.shoes + '?' + query);
        },
        shoes_filters() {
            return http.get(urls.shoes_filters);
        },
        get_shoe_detail(id) {
            return http.get(urls.shoes + id + '/');
        }
    },
    cart: {
        check_out(payload) {
            return http.post(urls.check_out, payload);
        }
    }
}