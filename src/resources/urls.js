const urls = {
    shoes: '/shoe/shoes/',
    shoes_filters: '/shoe/shoe-filter-data',

    check_out: '/cart/transactions/',
}

export default urls;